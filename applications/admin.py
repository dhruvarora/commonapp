# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from .models import Application, ApplicationPayment

class ApplicationPaymentTabularInline(admin.TabularInline):
    model = ApplicationPayment
    extra = 1

class ApplicationAdmin(admin.ModelAdmin):
    list_display = ["student", "university", "course", "application_id", "submitted", "admission_status"]
    list_filter = ["submitted", "admission_status"]
    readonly_fields = ["admission_status"]
    search_fields = ["student__first_name", "student__last_name", "university__name", "application_id"]
    inlines = [ApplicationPaymentTabularInline]

admin.site.register(Application, ApplicationAdmin)
admin.site.register(ApplicationPayment)
