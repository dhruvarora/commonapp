# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User
from colleges.models import College, Course, University

from commonapp import settings

now = timezone.now()

class Application(models.Model):
    student = models.ForeignKey(User)
    university = models.ForeignKey(University)
    course = models.ForeignKey(Course)
    application_id = models.CharField(max_length=75)
    submitted = models.BooleanField(default=False, choices=settings.YESNO_CHOICES)
    appfee_paid = models.BooleanField(default=False, choices=settings.YESNO_CHOICES)
    admission_status = models.CharField(max_length=150, choices=settings.ADMISSION_STATUSES)

    def __str__(self):
        return self.student.first_name + " application for " + self.university.name + " for course: " + self.course.name + " vide application id:" + self.application_id


class ApplicationPayment(models.Model):
    application = models.ForeignKey(Application)
    transaction_id = models.CharField(max_length=150)
    payment_status = models.CharField(max_length=20, choices=(
        ("Failed", "Failed"),
        ("Succesfull", "Succesfull")
    ))
    payment_method = models.CharField(max_length=50, default="Prepaid-CCAV")

    def __str__(self):
        return self.transaction_id
