# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User

now = timezone.now()

class Course(models.Model):
    date_added = models.DateField(default=now)
    name = models.CharField(max_length=150)

    def __str__(self):
        return self.name

class University(models.Model):
    date_added = models.DateField(default=now)
    administrators = models.ManyToManyField(User)
    name = models.CharField(max_length=150)
    address = models.TextField()
    city = models.CharField(max_length=30)
    state = models.CharField(max_length=30)
    application_fee = models.FloatField()

    def __str__(self):
        return self.name

class College(models.Model):
    administrators = models.ManyToManyField(User)
    date_added = models.DateField(default=now)
    name = models.CharField(max_length=150)
    address = models.TextField(blank=True, null=True)
    university = models.ForeignKey(University)
    courses = models.ManyToManyField(Course)

    def __str__(self):
        return self.name
