# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-04-07 07:36
from __future__ import unicode_literals

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('colleges', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='university',
            name='city',
            field=models.CharField(default='Delhi', max_length=30),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='university',
            name='state',
            field=models.CharField(default='Delhi', max_length=30),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='college',
            name='date_added',
            field=models.DateField(default=datetime.datetime(2017, 4, 7, 7, 36, 33, 324000, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='course',
            name='date_added',
            field=models.DateField(default=datetime.datetime(2017, 4, 7, 7, 36, 33, 324000, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='university',
            name='date_added',
            field=models.DateField(default=datetime.datetime(2017, 4, 7, 7, 36, 33, 324000, tzinfo=utc)),
        ),
    ]
