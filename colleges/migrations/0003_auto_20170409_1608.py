# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-04-09 10:38
from __future__ import unicode_literals

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('colleges', '0002_auto_20170407_1306'),
    ]

    operations = [
        migrations.AddField(
            model_name='university',
            name='application_fee',
            field=models.FloatField(default=1000),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='college',
            name='date_added',
            field=models.DateField(default=datetime.datetime(2017, 4, 9, 10, 38, 23, 824000, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='course',
            name='date_added',
            field=models.DateField(default=datetime.datetime(2017, 4, 9, 10, 38, 23, 824000, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='university',
            name='date_added',
            field=models.DateField(default=datetime.datetime(2017, 4, 9, 10, 38, 23, 824000, tzinfo=utc)),
        ),
    ]
