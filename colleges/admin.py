# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from .models import Course, University, College

class CourseAdmin(admin.ModelAdmin):
    list_display = ["date_added", "name"]
    search_fields = ["name"]

class UniversityAdmin(admin.ModelAdmin):
    list_display = ["date_added", "name", "city", "state", "address"]
    list_filter = ["state"]
    list_display_links = ["name"]
    search_fields = ["name"]


class CollegeAdmin(admin.ModelAdmin):
    list_display = ["date_added", "name", "university", "address"]
    list_filter = ["university__name"]
    search_fields = ["name", "university__name", "courses__name"]
    list_display_links = ["name"]

admin.site.register(Course, CourseAdmin)
admin.site.register(University, UniversityAdmin)
admin.site.register(College, CollegeAdmin)
