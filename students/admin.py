# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from .models import Profile, FamilyProfile, Institutes, Exams, Documents, Subjects

class ProfileAdmin(admin.ModelAdmin):
    def get_email(obj):
        return obj.user.email

    list_display = ["user", get_email, "aadhar", "sex", "date_of_birth", "city", "state"]
    list_filter = ["state", "sex"]
    search_fields = ["user__first_name", "user__last_name", "user__email", "mobile", "landline"]

admin.site.register(Profile, ProfileAdmin)
admin.site.register(FamilyProfile)
admin.site.register(Institutes)
admin.site.register(Exams)
admin.site.register(Documents)
admin.site.register(Subjects)
