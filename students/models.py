# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User as Student


class Profile(models.Model):
    user = models.OneToOneField(Student)
    aadhar = models.CharField(max_length=250)
    date_of_birth = models.DateField()
    sex = models.CharField(max_length=10, choices=(
    ("Male", "Male"),
    ("Female", "Female"),
    ("Third Gender", "Third Gender"),
    ))
    address1 = models.CharField(max_length=150)
    address2 = models.CharField(max_length=150)
    city = models.CharField(max_length=30)
    state = models.CharField(max_length=30)
    pincode = models.BigIntegerField()
    mobile = models.BigIntegerField()
    landline = models.BigIntegerField()

    def __str__(self):
        return self.user.first_name + " " + self.user.last_name + " PK=" + str(self.user.pk)

class FamilyProfile(models.Model):
    user = models.OneToOneField(Student)
    father_name = models.CharField(max_length=50)
    father_phone = models.BigIntegerField(blank=True, null=True)
    father_occupation = models.CharField(max_length=50, blank=True, null=True)
    father_email = models.EmailField(blank=True, null=True, max_length=150)
    mother_name = models.CharField(max_length=50, blank=True, null=True)
    mother_phone = models.BigIntegerField(blank=True, null=True)
    mother_occupation = models.CharField(max_length=50, blank=True, null=True)
    mother_email = models.EmailField(blank=True, null=True, max_length=150)
    single_parent = models.BooleanField(default=False)
    number_of_siblings = models.IntegerField(default=0)

    def __str__(self):
        return self.user.first_name + " " + self.user.last_name + " pk=" + str(self.user.pk) + " Family Profile"

class Institutes(models.Model):
    user = models.ForeignKey(Student)
    institute_name = models.CharField(max_length=150)
    institute_address = models.TextField(blank=True, null=True)
    institute_affiliation = models.CharField(max_length=150)


class Exams(models.Model):
    user = models.ForeignKey(Student)
    exam_name = models.CharField(max_length=150)
    exam_year = models.CharField(max_length=150)
    average_marks = models.FloatField()
    institute = models.ForeignKey(Institutes)

class Subjects(models.Model):
    exam = models.ForeignKey(Exams)
    subject_name = models.CharField(max_length=150)
    obtained_marks = models.FloatField()
    total_marks = models.FloatField()
    average_percentage = models.FloatField()

class Documents(models.Model):
    user = models.ForeignKey(Student)
    document_id = models.CharField(max_length=150, blank=True, null=True)
    document_name = models.CharField(max_length=150)
    document_file = models.FileField(max_length=200, upload_to="student-documents")
    verified = models.BooleanField(default=False)
